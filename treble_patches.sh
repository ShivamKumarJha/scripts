#!/bin/bash

cd lineage/

cd device/phh/treble/
chmod +x generate.sh
./generate.sh lineage
cd ../../../

Common () {
git apply *.patch
rm -rf *.patch
}

cd build/make
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_build/0001-Add-PRODUCT_SYSTEM_DEFAULT_PROPERTIES.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_build/0002-Move-ART-default-properties-into-system-partition.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_build/0003-Set-ro.build.fingerprint-in-system-etc-prop.default.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_build/0004-Revert-Remove-root-folder-bt_firmware-in-GSI.patch
Common
cd ../../external/selinux
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_external_selinux/0001-libsepol-cil-Add-ability-to-redeclare-types-attribut.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_external_selinux/0002-libsepol-cil-Keep-type-attribute-declarations-when-a.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_external_selinux/0003-libsepol-cil-Create-new-keep-field-for-type-attribut.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_external_selinux/0004-Enable-multipl_decls-by-default.-This-is-needed-beca.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_external_selinux/0005-libsepol-cil-Improve-processing-of-context-rules.patch
Common
cd ../../frameworks/base
wget https://github.com/AospExtended/platform_frameworks_base/commit/3e5fb377a36947c2732512c7c9732791a9f60578.patch
git apply --reverse *.patch
rm -rf *.patch
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_frameworks_base/0001-renderthread-relax-error-handling-for-wide-gamut-EGL.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_frameworks_base/0002-Reintroduce-button-backlight-and-respective-inactivi.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_frameworks_base/0003-power-Disable-keyboard-button-lights-while-dozing-dr.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_frameworks_base/0004-Fix-backlight-control-on-Galaxy-S9.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_frameworks_base/0005-Relax-requirement-for-visible-flag-to-sdcards.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_frameworks_base/0006-Include-gapps-modifications-for-webview.patch
Common
cd ../native
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_frameworks_native/0001-device-Huawei-HWC-doesn-t-understand-0-0-0-0-fullscr.patch
Common
cd ../opt/telephony
#wget https://github.com/phhusson/platform_frameworks_opt_telephony/commit/13fdd293f6a64f19900076bd79675f25c117b2a6.patch
#Common
cd ../../../system/bt
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_system_bt/0001-Make-BTM_BYPASS_EXTRA_ACL_SETUP-dynamic.patch
Common
cd ../core
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_system_core/0001-Revert-logd-add-passcred-for-logdw-socket.patch
Common
cd ../vold
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_system_vold/0001-Allow-deletion-of-symlink.patch
Common
wget https://raw.githubusercontent.com/phhusson/treble_patches/master/patches/platform_system_vold/0002-Workaround-perdev_minors-path-change-in-Linux.patch
Common
cd ../../

exit
